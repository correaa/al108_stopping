set term pdf
set output "al108_td.pdf"

set style data lines

plot \
	'al108_td_v0.100000.dat', \
	'al108_td_v0.200000.dat', \
	'al108_td_v0.300000.dat', \
	'al108_td_v0.400000.dat', \
	'al108_td_v0.700000.dat', \
	'al108_td_v0.900000.dat', \
	'al108_td_v1.000000.dat', \
	'al108_td_v1.200000.dat', \
	'al108_td_v1.400000.dat', \
	'al108_td_v1.600000.dat', \
	'al108_td_v1.800000.dat', \
	'al108_td_v2.000000.dat', \
	'al108_td_v2.500000.dat'  \
;


